package com.example.aviatorpop

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.aviatorpop.databinding.ActivityMainBinding
import com.example.aviatorpop.game.AviatorPopViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var bind : ActivityMainBinding
    private lateinit var gameViewModel: AviatorPopViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bind = ActivityMainBinding.inflate(layoutInflater)
        gameViewModel = ViewModelProvider(this)[AviatorPopViewModel::class.java]

        setContentView(bind.root)
    }

    override fun onResume() {
        val gameState = gameViewModel.getGameState()
        gameState?.let {
            bind.game.setGameState(it)
        }
        super.onResume()
    }

    override fun onPause() {
        gameViewModel.saveGameState(bind.game.getState())
        super.onPause()
    }
}