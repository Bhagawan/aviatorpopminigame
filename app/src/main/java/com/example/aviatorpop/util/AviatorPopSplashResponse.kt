package com.example.aviatorpop.util

import androidx.annotation.Keep

@Keep
data class AviatorPopSplashResponse(val url : String)