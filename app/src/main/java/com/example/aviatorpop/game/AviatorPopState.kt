package com.example.aviatorpop.game

data class AviatorPopState(val planeY: Float, val distance: Int, val targets: List<GameTarget>, val fuel: Int, val state: Int)
