package com.example.aviatorpop.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.aviatorpop.R
import com.example.aviatorpop.util.AppPreferences
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random

class AviatorPopView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeHeight = 1.0f
    private var planeWidth = 1.0f

    private var distance = 0

    private val planeBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.plane)
    private val restartBitmap = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()

    private val targets = ArrayList<GameTarget>()
    private var maxTargetRadius = 100
    private var minTargetRadius = 40

    private var planeFuel = 60

    private var state = START

    companion object {
        const val START = 0
        const val GAME = 1
        const val FINISH = 2
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeY = mHeight / 2.0f
            planeX = mWidth / 2.0f
            planeHeight = mWidth / 15.0f
            planeWidth = planeHeight / planeBitmap.height * planeBitmap.width
            minTargetRadius = mHeight / 25
            maxTargetRadius = mHeight / 15
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == GAME) {
                updatePlane()
                updateTargets()
                drawTargets(it)
            }
            drawTerrain(it)
            drawPlane(it)
            drawUI(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> return true
            MotionEvent.ACTION_UP -> {
                when(state) {
                    GAME -> hit(event.x, event.y)
                    else -> restart()
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun getState() : AviatorPopState = AviatorPopState(planeY, distance, targets, planeFuel, state)

    fun setGameState(gameState: AviatorPopState) {
        planeY = gameState.planeY
        targets.clear()
        targets.addAll(gameState.targets)
        planeFuel = gameState.fuel
        state = gameState.state
    }

    //// Private

    private fun drawPlane(c: Canvas) {
        val p = Paint()
        c.drawBitmap(planeBitmap, null, Rect((planeX - (planeWidth / 2)).toInt(), (planeY - planeHeight / 2).toInt(), (planeX + planeWidth / 2).toInt(), (planeY + planeHeight / 2).toInt()), p)
    }

    private fun drawTerrain(c : Canvas) {
        val p = Paint()
        p.color = ResourcesCompat.getColor(context.resources, R.color.green, null)
        p.style = Paint.Style.FILL
        c.drawRect(0.0f, mHeight - 10.0f, mWidth.toFloat(), mHeight.toFloat(), p)
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 30.0f
        p.textAlign = Paint.Align.CENTER

        when(state) {
            START -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.2f, mWidth * 0.85f, mHeight * 0.2f + 150, 20f, 20f, p)
                p.color = Color.WHITE
                c.drawText("Нажимайте на круги,", mWidth / 2.0f, mHeight * 0.2f + 50, p)
                c.drawText("чтобы поддерживать самолет", mWidth / 2.0f, mHeight * 0.2f + 100 , p)
                p.color = Color.BLACK
                c.drawRoundRect(mWidth * 0.5f - planeWidth - 20, mHeight * 0.2f + 170, mWidth * 0.5f + planeWidth + 20, mHeight * 0.2f + 330 + planeHeight, 20f, 20f, p)
                c.drawBitmap(planeBitmap, null, Rect(((mWidth - planeWidth) * 0.5f).toInt(), (mHeight * 0.2f + 200).toInt(), ((mWidth + planeWidth) * 0.5f).toInt(),  (mHeight * 0.2f + 200 + planeHeight).toInt()), p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.orange, null)
                c.drawCircle(mWidth / 2.0f, mHeight * 0.2f + 250 + planeHeight, 40.0f, p)
            }
            GAME -> {
                p.color = Color.WHITE
                p.textSize = 30.0f
                p.isFakeBoldText = true
                c.drawText((distance / 10.0f).toInt().toString(), mWidth / 2.0f, 30.0f, p)
            }
            FINISH -> {
                p.strokeWidth = 3.0f
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                p.style = Paint.Style.FILL
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 50, mWidth * 0.85f, mHeight * 0.3f + 100, 20f, 20f, p)
                p.color = ResourcesCompat.getColor(context.resources, R.color.orange, null)
                p.style = Paint.Style.STROKE
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 50, mWidth * 0.85f, mHeight * 0.3f + 100, 20f, 20f, p)
                p.style = Paint.Style.FILL
                val pad = (restartBitmap?.width ?: 90) * 0.5f + 5
                c.drawRoundRect(mWidth * 0.5f - pad, mHeight * 0.7f - pad, mWidth * 0.5f + pad, mHeight * 0.7f + pad, 20f, 20f, p)

                p.color = Color.WHITE
                c.drawText("Дистанция: ${distance / 10}", mWidth * 0.5f, mHeight * 0.3f, p)
                p.color = Color.YELLOW
                p.isFakeBoldText = true
                c.drawText("Рекорд: ${AppPreferences.loadRecord(context)}", mWidth * 0.5f, mHeight * 0.3f + 50, p)

                p.style = Paint.Style.STROKE
                p.color = ResourcesCompat.getColor(context.resources, R.color.orange, null)
                c.drawRoundRect(mWidth * 0.5f - pad + 5, mHeight * 0.7f - pad + 5, mWidth * 0.5f + pad - 5, mHeight * 0.7f + pad - 5, 20f, 20f, p)

                restartBitmap?.let { c.drawBitmap(it, (mWidth - it.width) * 0.5f, mHeight * 0.7f - it.height * 0.5f, p) }

            }
        }
    }

    private fun drawTargets(c : Canvas) {
        val p = Paint()
        for(target in targets) {
            if(target.pressed) {
                p.color = ResourcesCompat.getColor(context.resources, R.color.green, null)
            } else {
                val part : Float = (target.lifespan - target.liveTime).coerceAtLeast(0).toFloat() / target.lifespan
                p.color = Color.argb(255, 228,(123 * part).toInt(),(43 * part).toInt())
            }
            c.drawCircle(target.x.toFloat(), target.y.toFloat(), target.radius.toFloat(), p)
        }
    }

    private fun updatePlane() {
        planeFuel = (planeFuel - 1).coerceAtLeast(0)
        distance++
        if(planeFuel <= 0) planeY++
        if(planeY > mHeight - planeHeight / 2) {
            state = FINISH
            if(AppPreferences.loadRecord(context) < distance / 10) AppPreferences.saveRecord(context, distance / 10)
        }
    }

    private fun updateTargets() {
        var n = 0
        while(n < targets.size) {
            targets[n].liveTime++
            if(targets[n].pressed) targets[n].radius++
            if(targets[n].liveTime > targets[n].lifespan) {
                targets.removeAt(n)
                n--
            }
            n++
        }

        if(targets.isEmpty() || Random.nextInt(1000) < 8) spawnTarget()
    }

    private fun spawnTarget() {
        var x  = Random.nextInt(maxTargetRadius, mWidth - maxTargetRadius)
        var y  = Random.nextInt(maxTargetRadius + 50, mHeight - maxTargetRadius - 10)
        while(x in (planeX - planeWidth / 2 - maxTargetRadius).toInt()..(planeX + planeWidth / 2 + maxTargetRadius).toInt() &&
            y in (planeY - planeHeight / 2 - maxTargetRadius).toInt()..(planeY + planeHeight / 2 + maxTargetRadius).toInt()) {

            x  = Random.nextInt(maxTargetRadius, mWidth - maxTargetRadius)
            y  = Random.nextInt(maxTargetRadius + 50, mHeight - maxTargetRadius - 10)
        }
        val lifespan = 20 + (180 - distance / 50).coerceAtLeast(0)
        targets.add(GameTarget(x, y, Random.nextInt(minTargetRadius, maxTargetRadius), lifespan))
    }

    private fun hit(x: Float, y: Float) {
        for(target in targets) {
            if((target.x - x).absoluteValue < target.radius && (target.y - y).absoluteValue < target.radius && !target.pressed) {
                target.pressed = true
                target.lifespan = target.liveTime + 30
                planeFuel+=50
            }
        }
    }

    private fun restart() {
        state = GAME
        planeY = mHeight / 2.0f
        distance = 0
        planeFuel = 60
        targets.clear()
    }
}