package com.example.aviatorpop.game

import androidx.lifecycle.ViewModel

class AviatorPopViewModel: ViewModel(){
    private var gameState: AviatorPopState? = null

    fun saveGameState(state: AviatorPopState) {
        gameState = state
    }

    fun getGameState() : AviatorPopState? = gameState
}
